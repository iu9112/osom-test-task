export interface Change {
  amount: string;
  percents: string;
  isUptrend: boolean;
}
export interface ChartData {
  change: Change;
  priceData?: ((number)[] | null)[] | null;
}

export interface Alert {
  targetPriceEur: string;
  percentsToChange: string;
  isHigher: boolean;
}

export interface PriceHistory {
  '1D'?: ChartData;
  '7D'?: ChartData;
  '1M'?: ChartData;
  '3M'?: ChartData;
  'YTD'?: ChartData;
  'ALL'?: ChartData;
}

export interface Currency {
  name: string;
  symbol: string;
  price: string;
  circulatingSupply: string;
  maxSupply: string;
  marketCap: string;
  allTimeHigh: string;
  description: string;
  alerts?: (Alert)[] | null;
  priceHistory: PriceHistory;
}

export interface CurrencyInfo {
  name: string;
  amount: string;
  amountEur: string;
  symbol: string;
}
