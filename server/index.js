const express = require('express'),
    http = require('http'),
    fs = require('fs'),
    currenciesList = require('./data/currencies-list.json');

const app = express(),
  port = process.env.PORT || 4000,
  server = http.Server(app);

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.send(JSON.stringify(currenciesList));
})

app.get('/currency/:slug', function(req , res){
  fs.readFile('./data/assets-data.json', (err, data) => {
    if (err) throw err;
    const currency = JSON.parse(data);
    const filtered = currency.find( ({ symbol }) => symbol === req.params.slug );
    res.header("Access-Control-Allow-Origin", "*");
    if(filtered) {
      res.send(filtered);
    } else {
      res.sendStatus(404);
    }
  });
});

server.listen(port, function(){
  console.log('Listening on port ' + port);
});